import sys
sys.path.append('/opt/python3.6/lib/python3.6/site-packages')

from pynq.overlays.microblaze8 import BaseOverlay
overlay = BaseOverlay("microblaze8.bit")

if overlay.is_loaded():
    print("Overlay loaded")
