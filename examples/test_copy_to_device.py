from epython import *

model_W1=None
model_W2=None

define_on_device(model_W1)
define_on_device(model_W2)

W1 = [0,1,2,3,4,5,6,7,8,9]
W2 = [0,1,2,3,4,5,6,7,8,9]

@offload
def initCore():
	model_W1 = array(10)
	model_W2 = array(10)

@offload
def printArray():
	print model_W1[5]
	print model_W2[5]

def copyModelToMicroBlaze():
	i=0
	handlers=KernelExecutionHandler()
	while i<8:
		#handlers.append(copy_to_device("model_W1", W1, target=i, async=True))
		i+=1
	handlers.wait()
	copy_to_device("model_W2", W2)

initCore()
copyModelToMicroBlaze()
printArray()
