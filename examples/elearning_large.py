import numpy as np
import csv
import sys
import scipy.ndimage
import cPickle as pickle
import warnings
import time
from datetime import date
import math
import random as rd

use_numpy = True

#sys.stdout = open('ml_el_large_'+date.today().isoformat()+'.txt', 'w')

print "Initialising..."
H = 100
D = 15*15*16
#WIDTH=48
#HEIGHT=48
#D = WIDTH*HEIGHT*48
#D = 192*192*192
learning_rate=1e-3
batch_size=10
decay_rate = 0.99

coreID=0

model_W1=None
model_W2=None
new_W1=None
new_W2=None
h_data=None

localImage=[0.0]*225
handle=None

image=[0.0]*7077888
#initial_W1=[0.0]*11059200
initial_W1=[0.0]*H*D
initial_W1=[0.0]*110
initial_W2=[0.0]*100

#initial_W1=[0.0]*H*D
#initial_W2=[0.0]*H
#initial_W1[:]=[np.random.standard_normal() for x in initial_W1] / np.sqrt(D)
#initial_W1[:]=[ x/np.sqrt(D) for x in [rd.gauss(0,0.5) for x in initial_W1]] # / np.sqrt(D)
initial_W1[:] = np.random.randn(H*D) / np.sqrt(D) # "Xavier" initialization
initial_W2[:] = np.random.randn(H) / np.sqrt(H)

#define_on_device(model_W1)
#define_on_device(model_W2)
#define_on_device(new_W1)
#define_on_device(new_W2)
#define_on_device(h_data)
#define_on_device(learning_rate)
#define_on_device(localImage)
#define_on_device(handle)
#share_with_device(image)
#share_with_device(initial_W1)
#share_with_device(initial_W2)

def initCores():
	global model_W1, model_W2, new_W1, new_W2, h_data, localImage
	width=100*225
	model_W1=[0.0]*width
	model_W2=[0.0]*100	
	new_W1=[0.0]*width
	new_W2=[0.0]*100
	h_data=[0.0]*100
	localImage=[0.0]*225
	i=0
	idx=0
	while i<100:
		j=0
		new_W2[i]=0.0
		while j<225:
			idx=i*225+j
			new_W1[idx]=0.0
			j+=1
		i+=1

#def dotProduct(A,b):
#  result=np.zeros(A.shape[0])
#  i=0
  #while i<A.shape[0]:
#  while i<100:
#    j=0
    #while j<len(b):
#    while j<225:
#      result[i]+=A[i*225+j]*b[j]
#      j+=1
#    i+=1
#  return result

def dotProductVector(b,bp):
  result=0.0
  max_len=len(bp)
  j=0
  while j<max_len:
    result=result+b[j]*bp[j]
    j+=1
  return result

def calculateW2contributions(s):
  i=0
  maxlen=len(h_data)
  while i<maxlen:
    d1=h_data[i]
    d1*=s
    new_W2[i]=new_W2[i]+d1
    i+=1

def calculateW1contributions(dlogp, image):
	#sp=shape(new_W1)
	i=0
	val=0.0
	#while i<sp[0]:
	while i<100:
		j=0
		if h_data[i]>0.0:
			val=model_W2[i] 
			#val*=dlogp
		else:
			val=0.0
		#while j<sp[1]:
		while j<225:
			d1=image[j]
			d1*=val
			new_W1[i*225+j]=new_W1[i*225+j] + d1
			j+=1
		i+=1

def preprocess(x):
  xsize=x.shape[0]-1 if x.shape[0]-1<192 else 191
  ysize=x.shape[1]-1 if x.shape[1]-1<192 else 191
  zsize=x.shape[2]-1 if x.shape[2]-1<192 else 191
  fullsized=np.zeros([192,192,192])
  fullsized[0:xsize,0:ysize,0:zsize]=x[0:xsize,0:ysize,0:zsize]
  return fullsized.astype(np.float)

def sigmoid(x):
  return 1.0 / (1.0 + np.exp(-x))

def ep_dotProduct(A, b):
  global h_data
  #sp=A.shape
  i=0
  #while i<sp[0]:
  while i<100:
    j=0
    h_data[i]=0.0
    #while j<sp[1]:
    while j<225:
      d1=A[i*225+j]
      d2=b[j]
      d3=d1*d2
      h_data[i]=h_data[i]+d3#(A[i][j]*b[j])
      j+=1
    if (h_data[i] < 0.0): h_data[i]=0.0
    i+=1

def ep_policy_forward(offset):
	global localImage, image, model_W1, model_W2, h_data, coreID
	#handle=async_get_slice(image,offset,225)
	#wait(handle)
	#value_byref(handle,localImage,0)	
	#requestImage(offset+225)		
	#getImage(offset+225)	
	#offset=offset+coreID*442368	# image slice size
	logp = 0.0
	localImage = image[offset:offset+225] 
	#print offset, len(image), len(localImage)
	ep_dotProduct(model_W1,localImage) 
	logp = dotProductVector(model_W2, h_data)
	return logp

def policy_forward(offset):
	global coreID
	logp=[]
	coreID = 0
	while coreID < 15:	
			logp.append(ep_policy_forward(offset))
			coreID+=1 
	p=sigmoid(sum(logp))
	return p

def ep_combine_gradient(reward, dlogp): 
	global localImage
#	handle=async_get_slice(image,offset,225)
	calculateW2contributions(reward)
#	wait(handle)
#	value_byref(handle,localImage,0)	
	calculateW1contributions(dlogp, localImage)

def combine_gradient(reward, dlogp, image):
	coreID = 0
	while coreID < 15:	
    		ep_combine_gradient(reward, dlogp)   
		coreID+=1 


def update_model():
	global model_W1, model_W2, new_W1, new_W2
	#sp=shape(model_W1)
	i=0
	idx=0
	#while i<sp[0]:
	while i<100:
		j=0
		d1=new_W2[i]
		d1*=learning_rate
		model_W2[i]=model_W2[i]-d1 #(learning_rate*new_W2[i])
		new_W2[i]=0.0
		#while j<sp[1]:
		while j<225:
			idx=i*225+j
			d2=new_W1[idx]
			d2*=learning_rate
			model_W1[idx]=model_W1[idx]-d2 #(learning_rate * new_W1[i][j])
			new_W1[idx]=0.0
			j+=1
		i+=1

def getW1():
	#offset=coreid()*225
	#myHandle = async_get_slice(initial_W1,offset,225)
	#wait(myHandle)
	#value_byref(myHandle,model_W1,0)
	model_W1=initial_W1[coreID*225:coreID*225+225]

def getW2():
	#myHandle = async_get_slice(initial_W2,0,100)
	#wait(myHandle)
	#value_byref(myHandle,model_W2,0)
	model_W2=initial_W2[0:100]
	
def getImage(pos):
	offset=pos+coreID*442368	# image slice size
	#offset=pos+coreid()*225	# image slice size
	#handle=async_get_slice(image,offset,225)
	#wait(handle)
	#value_byref(handle,localImage,0)	
	localImage=image[offset:offset+225]

initCores()
print "Transferring W1..."
getW1()
print "Transferring W2..."
getW2()
print "Done init on Epiphany"

overall_images=[]
overall_cancer=[]
csvfile=open("examples/learning/stage1_labels_large.csv", 'r')
reader = csv.DictReader(csvfile)
for row in reader:
  overall_images.append(row['id'])
  overall_cancer.append(row['cancer'])

csvfile.close()

split_size = int(len(overall_images)*0.7)
training_images=np.array(overall_images[:split_size])
training_images.resize((split_size/batch_size)+1, batch_size)
training_cancer=np.array((overall_cancer[:split_size]))
training_cancer.resize((split_size/batch_size)+1, batch_size)

test_images=np.array(overall_images[split_size:])
test_cancer=np.array((overall_cancer[split_size:]))

def trainModel():
  global image, coreID
  for batch_ids, batch_cancer in zip(training_images, training_cancer):
    for t, cancer in zip(batch_ids, batch_cancer):
      if (t != 0):
	image[:]=preprocess(np.load('examples/learning/proc_data/'+t+".npy")).ravel().tolist()
	offset=0
	logp=0.0
	total_forward_time=0.0
	total_combine_gradients_time=0.0
	#while offset<1125:
	# image slice size
	count=0
	startImage = time.clock()
	while offset<442368:
	#while offset<300:
		#print("[%6d]\b\b\b\b\b\b\b\b\b"%offset),
		#sys.stdout.flush()	
		#requestImage(offset)	
		start = time.clock()
		aprob = policy_forward(offset)
		#total_forward_time.append(time.clock()-start)
		total_forward_time = (time.clock()-start)
		action = 1 if np.random.uniform() < aprob else 0
		reward=1 if action == cancer else 0
		start = time.clock()
		dlogp=action - aprob
		combine_gradient(reward, dlogp, image)
		#total_combine_gradients_time.append(time.clock()-start)
		total_combine_gradients_time=(time.clock()-start)
		offset=offset+225	# use for blocking comms
		#offset=offset+450	# use for non-blocking comms
		print str(count)+":Forward avg time:"+str(total_forward_time)+":sec"
		print str(count)+":Combine gradients avg time:"+str(total_combine_gradients_time)+":sec"
	#print "Forward avg time="+str(sum(total_forward_time))+" sec"
        #print "Combine gradients avg time="+str(sum(total_combine_gradients_time))+" sec"
	total_image_time=(time.clock()-startImage)
        print str(count)+":Total image time:"+str(total_image_time)+":sec"
	count+=1
    print "Do model update"
    start = time.clock()
    update_model()
    print "Update model avg time="+str(time.clock()-start)+" sec"

def testModel():
  totalTested=0
  totalHits=0
  for t, cancer in zip(test_images, test_cancer):
    image=preprocess(np.load('proc_data/'+t+".npy")).ravel()
    aprob, h = policy_forward(image)
    action = 1 if np.random.uniform() < aprob else 0
    if action == cancer: totalHits+=1
    totalTested+=1
  return totalHits, totalTested

overallIteration=0


while True:
  with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    trainModel()
    #pickle.dump(model, open('model.p', 'wb'))
    hits,overall=testModel()
    overallIteration+=1
    print "Iteration "+str(overallIteration)+" with "+str(hits)+" out of "+str(overall)
    print "---------------------------------------------------"


