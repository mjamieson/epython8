#from epython import offload
from epython import *

a=None

define_on_device(a)

@offload
def set(x):
    a = x

@offload
def doit(x):
    #return a+coreid()
    return a

print 'Number of cores: ' + str(numcores())
set(42)
print sum(doit(2))
#pi=sum(doit(2))
#print "Value of x is "+str(pi)
