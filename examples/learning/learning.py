import numpy as np
import csv
import sys
import scipy.ndimage
import cPickle as pickle
import warnings
import time

use_numpy = False
H = 100
D = 15*15*16
learning_rate = 1e-3
batch_size=10
decay_rate = 0.99

#model = pickle.load(open('model.p', 'rb'))
model = {}
model['W1'] = np.random.randn(H,D) / np.sqrt(D) # "Xavier" initialization
model['W2'] = np.random.randn(H) / np.sqrt(H)

dW1=np.zeros([H,D])
dW2=np.zeros(H)

def dotProduct(A,b):
  result=np.zeros(A.shape[0])
  i=0
  while i<A.shape[0]:
    j=0
    while j<len(b):
      result[i]+=A[i][j]*b[j]
      j+=1
    i+=1
  return result

def dotProductVector(b,bp):
  result=0.0
  j=0
  while j<len(bp):
    result+=b[j]*bp[j]
    j+=1
  return result

def calculateW2contributions(dW2, b, s):
  i=0
  while i<len(b):
    dW2+=(b[i]*s)
    i+=1

def calculateW1contributions(dW1, hs, dlogp, image):
  i=0
  while i<dW1.shape[0]:
    j=0
    if hs[i]<=0:
      val=0
    else:
      val=model['W2'][i] * dlogp
    while j<dW1.shape[1]:
      dW1[i,j]+=val*image[j]
      j+=1
    i+=1

def preprocess(x):
  midsized=np.zeros([15,15,16])
  for i in range(0,15):
    midsized[i,:,:]=scipy.misc.imresize(x[i*2,:,:], (15, 16))
  return midsized.astype(np.float)

def sigmoid(x):
  return 1.0 / (1.0 + np.exp(-x))

def policy_forward(x):
  if (use_numpy):
    h=np.dot(model['W1'], x)
  else:
    h=dotProduct(model['W1'], x)
  h[h<0] = 0 # ReLU nonlinearity
  if (use_numpy):
    logp=np.dot(model['W2'], h)
  else:
    logp=dotProductVector(model['W2'], h)
  p=sigmoid(logp)
  return p, h

def combine_gradient(hs, reward, dlogp, image):
  global dW2, dW1
  if (use_numpy):
    dW2+=np.dot(hs, reward)
    dh=np.outer(dlogp, model['W2']).reshape(100)
    dh[hs <= 0] = 0
    image=image.reshape(1, len(image))
    dh=dh.reshape(len(dh), 1)
    dW1+=np.dot(dh, image)
  else:
    calculateW2contributions(dW2, hs, reward)
    calculateW1contributions(dW1, hs, dlogp, image)

def update_model():
  model['W1'] += -learning_rate * dW1
  model['W2'] += -learning_rate * dW2
  dW1.fill(0.0)
  dW2.fill(0.0)

overall_images=[]
overall_cancer=[]
csvfile=open("stage1_labels.csv", 'r')
reader = csv.DictReader(csvfile)
for row in reader:
  overall_images.append(row['id'])
  overall_cancer.append(row['cancer'])

csvfile.close()

split_size = int(len(overall_images)*0.7)
training_images=np.array(overall_images[:split_size])
training_images.resize(10, (split_size/batch_size)+1)
training_cancer=np.array((overall_cancer[:split_size]))
training_cancer.resize(10, (split_size/batch_size)+1)

test_images=np.array(overall_images[split_size:])
test_cancer=np.array((overall_cancer[split_size:]))

def trainModel():
  for batch_ids, batch_cancer in zip(training_images, training_cancer):
    for t, cancer in zip(batch_ids, batch_cancer):
      if (t != 0):
        image=preprocess(np.load('proc_data/'+t+".npy")).ravel()
        start = time.clock()
        aprob, h = policy_forward(image)
        total_forward_time=(time.clock()-start)
        action = 1 if np.random.uniform() < aprob else 0
        reward=1 if action == cancer else 0
        start = time.clock()
        combine_gradient(h, reward, action - aprob, image)
        total_combine_gradients_time=(time.clock()-start)
        print "Forward avg time="+str(total_forward_time)+" sec"
        print "Combine gradients avg time="+str(total_combine_gradients_time)+" sec"
    start = time.clock()
    update_model()
    print "Update model avg time="+str(time.clock()-start)+" sec"

def testModel():
  totalTested=0
  totalHits=0
  for t, cancer in zip(test_images, test_cancer):
    image=preprocess(np.load('proc_data/'+t+".npy")).ravel()
    aprob, h = policy_forward(image)
    action = 1 if np.random.uniform() < aprob else 0
    if action == cancer: totalHits+=1
    totalTested+=1
  return totalHits, totalTested

overallIteration=0

while True:
  with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    trainModel()
    #pickle.dump(model, open('model.p', 'wb'))
    hits,overall=testModel()
    overallIteration+=1
    print "Iteration "+str(overallIteration)+" with "+str(hits)+" out of "+str(overall)
    print "---------------------------------------------------"


