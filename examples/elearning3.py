from epython import *
import numpy as np
import csv
import sys
import scipy.ndimage
import cPickle as pickle
import warnings
import time
import math

use_numpy = True
H = 100
#D = 15*15*16
#D = 15*15*8
D = 15*15*3
learning_rate=1e-3
batch_size=10
decay_rate = 0.99

model_W1=None
model_W2=None
new_W1=None
new_W2=None
h_data=None

define_on_device(model_W1)
define_on_device(model_W2)
define_on_device(new_W1)
define_on_device(new_W2)
define_on_device(h_data)
define_on_device(learning_rate)

#model = pickle.load(open('model.p', 'rb'))
model = {}
model['W1'] = np.random.randn(H,D) / np.sqrt(D) # "Xavier" initialization
model['W2'] = np.random.randn(H) / np.sqrt(H)

dW1=np.zeros([H,D])
dW2=np.zeros(H)

@offload
def initCores():
	model_W1=array(100, 225)
	model_W2=array(100)
	new_W1=array(100, 225)
	new_W2=array(100)
	h_data=array(100)
	i=0
	while i<100:
		j=0
		new_W2[i]=0.0
		while j<225:
			new_W1[i][j]=0.0
			j+=1
		i+=1

def dotProduct(A,b):
  result=np.zeros(A.shape[0])
  i=0
  while i<A.shape[0]:
    j=0
    while j<len(b):
      result[i]+=A[i][j]*b[j]
      j+=1
    i+=1
  return result

@offload
def dotProductVector(b,bp):
  result=0.0
  max_len=len(bp)
  j=0
  while j<max_len:
    result=result+b[j]*bp[j]
    j+=1
  return result

@offload
def calculateW2contributions(s):
  i=0
  maxlen=len(h_data)
  while i<maxlen:
    d1=h_data[i]
    d1*=s
    new_W2[i]=new_W2[i]+d1
    i+=1

@offload
def calculateW1contributions(dlogp, image):
	sp=shape(new_W1)
	i=0
	val=0.0
	while i<sp[0]:
		j=0
		if h_data[i]>0.0:
			val=model_W2[i] 
			#val*=dlogp
		else:
			val=0.0
		while j<sp[1]:
			d1=image[j]
			d1*=val
			new_W1[i][j]=new_W1[i][j] + d1
			j+=1
		i+=1

def preprocess(x):
  #midsized=np.zeros([15,15,16])
  #midsized=np.zeros([15,15,8])
  midsized=np.zeros([15,15,3])
  #for i in range(0,15):
  for i in range(0,2):
    #midsized[i,:,:]=scipy.misc.imresize(x[i*2,:,:], (15, 16))
    #midsized[i,:,:]=scipy.misc.imresize(x[i*2,:,:], (15, 8))
    midsized[i,:,:]=scipy.misc.imresize(x[i*2,:,:], (15, 3))
  return midsized.astype(np.float)

def sigmoid(x):
  #print str(np.exp(-x))+" "+str(x)+" "+str(1.0 / (1.0 + np.exp(-x)))
  return 1.0 / (1.0 + np.exp(-x))

@offload
def ep_dotProduct(A, b):
  sp=shape(A)
  i=0
  while i<sp[0]:
    j=0
    h_data[i]=0.0
    while j<sp[1]:
      d1=A[i][j]
      d2=b[j]
      d3=d1*d2
      h_data[i]=h_data[i]+d3#(A[i][j]*b[j])
      j+=1
    if (h_data[i] < 0.0): h_data[i]=0.0
    i+=1

@offload
def ep_policy_forward(x):
	ep_dotProduct(model_W1, x)
	logp= dotProductVector(model_W2, h_data)
	return logp

def policy_forward(x):
	#print "Ready"
	handlers=KernelExecutionHandler()
	i=0
	#while i<16:
	#while i<8:
	while i<3:
		handlers.append(ep_policy_forward(x[i*225:(i+1)*225], async=True, target=i, all=False))
		i+=1
	logp=handlers.wait()
	#print logp
	p=sigmoid(sum(logp))
	return p

@offload
def ep_combine_gradient(reward, dlogp, image): 
	calculateW2contributions(reward)
	calculateW1contributions(dlogp, image)

def combine_gradient(reward, dlogp, image):
	handlers=KernelExecutionHandler()
	i=0
	#while i<16:
	#while i<8:
	while i<3:
		handlers.append(ep_combine_gradient(reward, dlogp, image[i*225:(i+1)*225], async=True, target=i, all=False))
		i+=1
	handlers.wait()

@offload
def update_model():
	sp=shape(model_W1)
	i=0
	while i<sp[0]:
		j=0
		d1=new_W2[i]
		d1*=learning_rate
		model_W2[i]=model_W2[i]-d1 #(learning_rate*new_W2[i])
		new_W2[i]=0.0
		while j<sp[1]:
			d2=new_W1[i][j]
			d2*=learning_rate
			model_W1[i][j]=model_W1[i][j]-d2 #(learning_rate * new_W1[i][j])
			new_W1[i][j]=0.0
			j+=1
		i+=1

def copyModelToMicroBlaze():
	i=0
	handlers=KernelExecutionHandler()
	#while i<16:
	#while i<8:
	while i<3:
		handlers.append(copy_to_device("model_W1", model['W1'][:,i*225:(i+1)*225].flatten().tolist(), target=i, async=True))
		i+=1
	handlers.wait()
	copy_to_device("model_W2", model['W2'].tolist())

initCores()
print "Initialised"
copyModelToMicroBlaze()

print "Done init on MicroBlaze"

overall_images=[]
overall_cancer=[]
csvfile=open("examples/learning/stage1_labels_large.csv", 'r')
reader = csv.DictReader(csvfile)
for row in reader:
  overall_images.append(row['id'])
  overall_cancer.append(row['cancer'])

csvfile.close()

split_size = int(len(overall_images)*0.7)
training_images=np.array(overall_images[:split_size])
training_images.resize((split_size/batch_size)+1, batch_size)
training_cancer=np.array((overall_cancer[:split_size]))
training_cancer.resize((split_size/batch_size)+1, batch_size)

test_images=np.array(overall_images[split_size:])
test_cancer=np.array((overall_cancer[split_size:]))

def trainModel():
  for batch_ids, batch_cancer in zip(training_images, training_cancer):
    for t, cancer in zip(batch_ids, batch_cancer):
      if (t != 0):
        image=preprocess(np.load('examples/learning/proc_data/'+t+".npy")).ravel().tolist()
#        print type(image[0])
        start = time.clock()
        aprob = policy_forward(image)
        total_forward_time=(time.clock()-start)
        action = 1 if np.random.uniform() < aprob else 0
        reward=1 if action == cancer else 0
        start = time.clock()
        dlogp=action - aprob
        combine_gradient(reward, dlogp, image)
        total_combine_gradients_time=(time.clock()-start)
        print "Forward avg time="+str(total_forward_time)+" sec"
        print "Combine gradients avg time="+str(total_combine_gradients_time)+" sec"
    print "Do model update"
    start = time.clock()
    update_model()
    print "Update model avg time="+str(time.clock()-start)+" sec"

def testModel():
  totalTested=0
  totalHits=0
  for t, cancer in zip(test_images, test_cancer):
    image=preprocess(np.load('proc_data/'+t+".npy")).ravel()
    aprob, h = policy_forward(image)
    action = 1 if np.random.uniform() < aprob else 0
    if action == cancer: totalHits+=1
    totalTested+=1
  return totalHits, totalTested

overallIteration=0


while True:
  with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    trainModel()
    #pickle.dump(model, open('model.p', 'wb'))
    hits,overall=testModel()
    overallIteration+=1
    print "Iteration "+str(overallIteration)+" with "+str(hits)+" out of "+str(overall)
    print "---------------------------------------------------"


